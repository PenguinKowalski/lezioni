using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimnetoAB : MonoBehaviour
{
    public Transform cuboA, cuboB; 
    Vector3 direzione;
    public float velocita = 1;
    // Update is called once per frame

    private void Start()
    {
        direzione = (cuboB.position - cuboA.position).normalized;
        this.transform.position = cuboA.position;
    }
    void Update()
    {
        this.transform.position += direzione * velocita * Time.deltaTime;
    }
}
