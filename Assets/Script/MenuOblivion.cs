using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MenuOblivion : MonoBehaviour
{
    public GameObject tastoStat, tastoMap, tastoBack, menuScreen, statsScreen, mapScreen;

    public Slider slider1, slider2, slider3;
    public float hp, mana, stamina;
    private void Start()
    {
        Back_OnClick();
    }

    public void Update()
    {
        slider1.value = hp;
        slider2.value = mana;
        slider3.value = stamina;
    }
    public void Back_OnClick()
    {
        tastoStat.SetActive(true);
        tastoMap.SetActive(true);
        menuScreen.SetActive(true);
        statsScreen.SetActive(false);
        mapScreen.SetActive(false);
        tastoBack.SetActive(false);
    }
    public void Stats_OnClick()
    {
        tastoStat.SetActive(false);
        tastoMap.SetActive(false);
        menuScreen.SetActive(false);
        statsScreen.SetActive(true);
        mapScreen.SetActive(false);
        tastoBack.SetActive(true);
    }
    public void Map_OnClick()
    {
        tastoStat.SetActive(false);
        tastoMap.SetActive(false);
        menuScreen.SetActive(false);
        statsScreen.SetActive(false);
        mapScreen.SetActive(true);
        tastoBack.SetActive(true);
    }
}
