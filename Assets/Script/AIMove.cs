using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIMove : MonoBehaviour
{
    NavMeshAgent agent;

    public Transform targetA, targetB;

    public Transform actualTarget;
    // Start is called before the first frame update
    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
        actualTarget = targetB;
    }

    // Update is called once per frame
    void Update()
    {
        if ((this.transform.position - targetA.position).magnitude < 1)
        {
            actualTarget = targetB;
        }

        if ((this.transform.position - targetB.position).magnitude < 1)
        {
            actualTarget = targetA;
        }
        //agent.Move();

        agent.SetDestination(actualTarget.position);
    }
}
