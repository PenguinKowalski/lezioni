using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Operazione{
    addizione, sottrazione, moltiplicazione, divisione, potenza, radice
}
public class Calcolatrice : MonoBehaviour
{
    [SerializeField] float numero1, numero2, numero3;
    [SerializeField] Operazione operazione;
   
    void Start()
    {
        float risultatoconto1 = Calcola(numero1, numero2, operazione);

    }

    public float Calcola(float _numero1, float _numero2, Operazione _operazione)
    {
        float risultato=0f;
        bool valido = true;

        switch (_operazione)
        {

            case (Operazione.addizione):
                risultato = _numero1 + _numero2;

                break;

            case (Operazione.sottrazione):
                risultato = _numero1 - _numero2;

                break;
            case (Operazione.moltiplicazione):
                risultato = _numero1 * _numero2;

                break;
            case (Operazione.divisione):

                if (_numero2 != 0)
                {
                    valido = false;
                    Debug.Log("il divisore � 0, non funziona");
                }
                else
                {
                    risultato = _numero1 / _numero2;
                }


                break;

            case (Operazione.potenza):
                if (numero2 < 100)
                {
                    risultato = Mathf.Pow(_numero1, _numero2);
                }
                else
                {
                    valido = false;
                    Debug.Log("la potenza sarebbe troppo grande da gestire");
                }
                break;
            case (Operazione.radice):
                if (_numero2 < 100)
                {
                    risultato = Mathf.Pow(_numero1, 1f / _numero2);
                }
                else
                {
                    valido = false;
                    Debug.Log("la radice sarebbe troppo piccola da gestire");
                }
                break;



        }

        if (valido)
        {
            Debug.Log("il risultato � " + risultato);

        }
        return risultato;
    }
}
