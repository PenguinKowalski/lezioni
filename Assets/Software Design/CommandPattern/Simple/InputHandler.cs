﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CommandPattern.Example1
{
    public class InputHandler : MonoBehaviour
    {


        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.D))
            {
                MoveRight();// se volessimo fargli fare un'altra azione?
            }
            if (Input.GetKeyDown(KeyCode.A))
            {
                MoveLeft();
            }
            if (Input.GetKeyDown(KeyCode.W))
            {
                MoveForward();
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                MoveBack();
            }
        }


        public void MoveRight()
        {
            this.transform.position += (new Vector3(1, 0, 0));
        }
        public void MoveLeft()
        {
            this.transform.position += (new Vector3(-1, 0, 0));
        }
        public void MoveForward()
        {
            this.transform.position += (new Vector3(0, 0, 1));
        }
        public void MoveBack()
        {
            this.transform.position += (new Vector3(0, 0, -1));
        }
    }
}

