﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SingletonPattern
{
    public class SingletonPattern : MonoBehaviour
    {
        private static SingletonPattern instance;
        public static SingletonPattern Get()
        {
            if (instance == null)
                instance = (SingletonPattern)FindObjectOfType(typeof(SingletonPattern));
            return instance;
        }

        public void DoSomething()
        {
            Debug.Log("Do something");
        }
    }
}
