﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace SingletonPattern
{
    public class SingletonUser : MonoBehaviour
    {

        void Start()
        {
            SingletonPattern.Get().DoSomething();
        }

    }
}
