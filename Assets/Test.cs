using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class Test : MonoBehaviour
{
    [SerializeField] int NumeroDiVolte=4, NumeroIniziale=7;
   

    private void Start()
    {
        int numero = NumeroIniziale;

        if (numero % 2 != 0)
        {
            numero++;
        }

        for (int i=0; i< NumeroDiVolte; i++)
        {
            Debug.Log("il numero ora � " + numero);
            numero = numero + 2;
           
        }
    }
}
