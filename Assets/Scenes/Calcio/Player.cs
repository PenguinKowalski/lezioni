using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Animator animator;
    public Rigidbody capsula;
    public float speed = 10f;
    float inputHorizontal;
    float inputVertical;
    Vector3 direzione; 


    private void Update()
    {
        inputVertical = Input.GetAxis("Vertical");
        direzione = new Vector3(inputHorizontal, 0, inputVertical);
        inputHorizontal = Input.GetAxis("Horizontal");

        if (Input.GetKeyDown(KeyCode.Space))
        {
            animator.SetTrigger("Kick");
        }
    }
    // Update is called once per frame
    void FixedUpdate()
    {
          
        capsula.position += direzione.normalized * speed;

        if (inputHorizontal !=0 || inputVertical != 0)
            capsula.transform.forward = direzione;

    }
    private void LateUpdate()
    {
        animator.SetFloat("Speed", direzione.magnitude);
    }

}
