using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    private void Awake()
    {
        GameManager[] gms= FindObjectsOfType<GameManager>();

        if (gms.Length > 1)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        } 
    }
    public int birilliCaduti = 0;
   
    public void CadutoBirillo()
    {
        birilliCaduti++;
    }

    public void FineRound()
    {
        Debug.Log("ho tirato giu " + birilliCaduti + "birilli");
        SceneManager.LoadScene(0);
    }

}
