using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingRound : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        GameManager.instance.FineRound();

    }
}
