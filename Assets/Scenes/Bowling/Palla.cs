using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Palla : MonoBehaviour
{
    public float positionMin, postionMax, angleMin, angleMax, forceMin, forceMax;
    public float mulForce;
    public int indiceStep = 0;

    public GameObject refPalla, refArrow;
    public Rigidbody rbPalla;

    public float force=0f, speed=2f;

    private void Start()
    {
        refArrow.SetActive(false);
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            indiceStep++;
            if (indiceStep > 0)
            {
                refArrow.SetActive(true);
            }
            if (indiceStep > 2)
            {
                refArrow.SetActive(false);

                Vector3 forzaLancio = refArrow.transform.right;
                rbPalla.AddForce(forzaLancio * -mulForce *force, ForceMode.Impulse );
            }
        }

       
        if (indiceStep == 0)
        {
            float t = Mathf.PingPong(Time.time * speed, 1);

            refPalla.transform.position = new Vector3(
                refPalla.transform.position.x,
                refPalla.transform.position.y,
                Mathf.Lerp(positionMin, postionMax, t)
                );
        }
        if (indiceStep == 1)
        {
            float t = Mathf.PingPong(Time.time * speed, 1);

            refArrow.transform.localRotation = Quaternion.Euler(
                new Vector3(
                90,
                0,
                Mathf.Lerp(angleMin, angleMax, t)
                ));
        }
        if (indiceStep == 2)
        {
           force = Mathf.PingPong(Time.time * speed, 1);

            refArrow.transform.localScale = new Vector3(
                Mathf.Lerp(forceMin, forceMax, force),
                refArrow.transform.localScale.y,
                refArrow.transform.localScale.z
                );
        }
    }
}
