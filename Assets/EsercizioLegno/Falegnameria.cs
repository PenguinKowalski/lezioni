using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Master
{
    public class Falegnameria : MonoBehaviour
    {
        public List<Legno> Inventario;


        public GameObject prefabBottoneLista, parenteLista, refPanelDescrizione, refCubo;
        Legno legnoSelezionato = null;

        public DescrizioneUI descrizioneUI;
        public CuboUi cuboUI;

        public Legno LegnoSelezionato { get => legnoSelezionato; set => legnoSelezionato = value; }

        void Start()
        {
            for (int i=0; i< Inventario.Count; i++)
            {
                GameObject go = GameObject.Instantiate(prefabBottoneLista, parenteLista.transform);
                ButtoneInventario button = go.GetComponent<ButtoneInventario>();

                button.Inizializzazione(this, Inventario[i]);
            }

            ExitButton();
        }
        public void EnterButton(Legno legnoVisualizzato)
        {
            refPanelDescrizione.SetActive(true);
            refCubo.SetActive(true);

            if (LegnoSelezionato == null)
            {
                cuboUI.Init(legnoVisualizzato);
                descrizioneUI.Init(legnoVisualizzato);
            }
               

        }

        public void ExitButton()
        {
            if (LegnoSelezionato == null)
            {
                refPanelDescrizione.SetActive(false);
                refCubo.SetActive(false);
            }

        }

        public void UpdateUI()
        {
            if (LegnoSelezionato == null)
            {
                refPanelDescrizione.SetActive(false);
                refCubo.SetActive(false);
            }
            else
            {
                refPanelDescrizione.SetActive(true);
                refCubo.SetActive(true);
                descrizioneUI.Init(LegnoSelezionato);
                cuboUI.Init(LegnoSelezionato);
            }
            
        }
        private void Update()
        {     
            if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
            {
                legnoSelezionato = null;
                UpdateUI();
            }
        }
    }
    [System.Serializable]
    public class Legno
    {
        public string Provenienza;
        public float lunghezza, larghezza;

        public override string ToString()
        {
            return Provenienza + " , " + lunghezza + "cm x"  + larghezza + "cm"; 
        }
    }



}
