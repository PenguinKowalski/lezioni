using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using Master;

public class ButtoneInventario : MonoBehaviour, IPointerExitHandler, IPointerEnterHandler
{
    public Falegnameria reference;
    public TextMeshProUGUI testoRef;
    Legno legnoBottone;
    public Button bottone;
    // Start is called before the first frame update

    public void Inizializzazione(Falegnameria fam, Legno legno)
    {
        reference = fam;
        legnoBottone = legno;
        testoRef.text = legno.ToString() ;

        bottone.onClick.AddListener(() =>
        {
            if (reference.LegnoSelezionato == legnoBottone)
            {
                reference.LegnoSelezionato = null;
            }
            else
            {
                reference.LegnoSelezionato = legnoBottone;
            }
          
            reference.UpdateUI();

        });
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        reference.EnterButton(legnoBottone);  
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        reference.ExitButton();
    }
}
