using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Master;
using UnityEngine.EventSystems;

public class CuboUi : MonoBehaviour
{
    public MeshRenderer rend;

    public List<Material> listaMateriali;

    public float f1, f2;
    public float sensibilita = 10f;
    public bool rotationactive = false;
    public void Init(Legno legnoSelezionato)
    {
        int indice = 0;
        if (legnoSelezionato.Provenienza=="Abete")
        {
            indice = 0;
        }
        else if (legnoSelezionato.Provenienza == "Quercia")
        {
            indice = 1;
        }
        else if (legnoSelezionato.Provenienza == "Pino")
        {
            indice = 2;
        }
        rend.sharedMaterial = listaMateriali[indice];

       // float rapporto = legnoSelezionato.larghezza / legnoSelezionato.lunghezza;
        // x:100 = 1 : 10  => x= 10;
        float x = legnoSelezionato.larghezza /  10f;
        float y = legnoSelezionato.lunghezza / 10f;
        transform.localScale = new Vector3(x, 0.2f, y);
        transform.localRotation = Quaternion.Euler(90, 0, 0);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && EventSystem.current.currentSelectedGameObject == this.gameObject)
        {
            rotationactive = true;
        }
        if (Input.GetMouseButtonUp(0))
        {
            rotationactive = false;
        }

        if (rotationactive)
        {
            f1 = Input.GetAxis("Mouse X");
            f2 = Input.GetAxis("Mouse Y");
            transform.Rotate(new Vector3(f1 * sensibilita * Time.deltaTime, f2 * sensibilita * Time.deltaTime, 0));
        }
      
    }

}
