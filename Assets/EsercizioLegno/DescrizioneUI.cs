using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Master;

public class DescrizioneUI : MonoBehaviour
{
    public TextMeshProUGUI descrizioneText, dimensioneText;


    public void Init(Legno legnoSelezionato)
    {
        descrizioneText.text = "questo legno viene da " +  legnoSelezionato.Provenienza;
        dimensioneText.text = legnoSelezionato.lunghezza + "cm X " + legnoSelezionato.larghezza + "cm";
    }

}
