using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycast : MonoBehaviour
{
    public float distanzaMax;
    // Start is called before the first frame update
    public LineRenderer lr;
    public List<Vector3> points= new List<Vector3>();
    public LayerMask layerMask;
    // Update is called once per frame
    void Update()
    {
        RaycastHit hit, hit2;
        points.Clear();
        points.Add(this.transform.position);
     //   Debug.DrawRay(this.transform.position, this.transform.forward* distanzaMax);
        if (Physics.Raycast(this.transform.position, this.transform.forward, out hit,distanzaMax))
        {
            Debug.DrawLine(this.transform.position, hit.point);
            Debug.Log("sto colpendo un oggetto " + hit.collider + "alla distanza" + hit.distance);
            Debug.DrawRay(hit.point, hit.normal, Color.magenta);
            points.Add(hit.point);
            Vector3 direzione = (hit.point - this.transform.position).normalized;

            Vector3 direzioneSpecchio= Vector3.Reflect(direzione, hit.normal).normalized;

           // Debug.DrawRay(hit.point, direzioneSpecchio*distanzaMax);
            if (Physics.Raycast(hit.point, direzioneSpecchio, out hit2, distanzaMax))
            {

                Debug.DrawLine(hit.point, hit2.point);
                Debug.Log("sto colpendo un oggetto " + hit2.collider + "alla distanza" + hit.distance);
                Debug.DrawRay(hit2.point, hit2.normal, Color.magenta);
             

            }
            points.Add(hit2.point);
        }
        /*
        int var;
        FunzioneTest(out var);
        */
        if (points.Count == 1)
        {
            lr.enabled = false;
        }
        else
        {
            lr.enabled = true;
            lr.positionCount = points.Count;
            lr.SetPositions(points.ToArray());
        }
       
    }

    public void FunzioneTest(out int variabiledacambiare)
    {
        variabiledacambiare = 5;
    }
}
