using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Esercizio4_lez1 : MonoBehaviour
{
    public List<Prodotto> listaProdotti = new List<Prodotto>();
    public List<Spesa> listaSpesa = new List<Spesa>();

    public Dictionary<string, Prodotto> TabellaProdotti = new Dictionary<string, Prodotto>();


    float Contanti = 100;
    float totale = 0;
    void Start()
    {
        for (int i = 0; i < listaProdotti.Count; i++)
        {
            TabellaProdotti.Add(listaProdotti[i].codice, listaProdotti[i]);
        }

        for (int i=0; i< listaSpesa.Count; i++)
        {
            Prodotto ProdLinkato = TrovaProdottoDaCodice(listaSpesa[i].codiceProdotto);
            if (ProdLinkato != null)
            {
                totale +=  ProdLinkato.prezzo; //prezzo;
            }
            else
            {
                Debug.Log("non ho trovato il prodotto" + listaSpesa[i].codiceProdotto);
            }
        }

        if (totale > Contanti)
        {
            Debug.Log("Ho speso piu' di quanto avevo");
        }
        else
        {
            Debug.Log("ho speso " + totale + ", mi rimangono " + (Contanti - totale));
        }

    }

    Prodotto TrovaProdottoDaCodice(string codice)
    {
        /*
        for (int i = 0; i < listaProdotti.Count; i++)
        {
            if (codice== listaProdotti[i].codice)
            {
                return listaProdotti[i];
            }
        }

        return null;
        */
        if (TabellaProdotti.ContainsKey(codice))
        {
            return TabellaProdotti[codice];
        }
        return null;
      
    }

}

[System.Serializable]
public class Prodotto
{
    public string codice, nome;
    public float prezzo; //per un prodotto
}
[System.Serializable]
public class Spesa
{
    public string codiceProdotto;
    public int quantita=1; //per un prodotto
}