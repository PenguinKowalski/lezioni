using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace esempioPoly
{
    [System.Serializable]
    public class Persona
    {
        public string nome;
        public string cognome;
        public int eta;

        public Persona(string nome, string cognome, int eta)
        {
            this.nome = nome;
            this.cognome = cognome;
            this.eta = eta;
        }

        public virtual string GetPersona()
        {
            return nome + " " + cognome + " ha " + eta + " anni";
        }
    }

}
