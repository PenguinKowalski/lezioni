using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Master
{
    public class Esercizio1 : MonoBehaviour
    {
        // Start is called before the first frame update
        public int N;
        int n1=0, n2=1 , risultato;

        private void Start()
        {
            if (N >= 1)
            {           
                Debug.Log(n1);
            }
            if (N >= 2)
            {       
                Debug.Log(n2);
            }         

            for (int i=2; i< N; i++)
            {
                risultato = n1 + n2;             
                n1 = n2;
                n2 = risultato;
                Debug.Log(risultato);
            }
        }

        
    }

}
