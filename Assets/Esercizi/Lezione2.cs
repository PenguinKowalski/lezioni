using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lezione2 : MonoBehaviour
{
    public Transform oggettodamuovere;

    public Vector3 posizioneIniziale;
    public Vector3 vettoreSpostamento;
    private void Awake()
    {
        oggettodamuovere.position = posizioneIniziale;
    }

    private void Update()
    {
        oggettodamuovere.position = oggettodamuovere.position + vettoreSpostamento;

    }
}
