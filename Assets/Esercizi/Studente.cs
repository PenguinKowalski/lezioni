using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace esempioPoly
{
    [System.Serializable]
    public class Studente : Persona
    {
        public string scuola;

        public Studente(string nome, string cognome, int eta, string scuola) : base(nome, cognome, eta)
        {
            this.nome = nome;
            this.scuola = scuola;
            this.eta = eta;
            this.cognome = cognome;
        }

        public override string GetPersona()
        {
            return  base.GetPersona() +  " e segue " + scuola;
        }

        public string GetStudente()
        {
            return GetPersona() + " e segue " + scuola;
        }
    }
}
