﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lezione10
{
    public class SaveLoad : MonoBehaviour
    {
        public float exampleFloat;
        public int exampleInt;
        public string exampleString;
        //these vaibles will collect the saved values
        string collectString;
        int collectInt;
        float collectFloat;

        void Save()
        { //this is the save function
            PlayerPrefs.SetString("exampleStringSave", exampleString);
            // the part in quotations is what the "bucket" is that holds your variables value
            // the second one in quotations is the value you want saved, you can also put a variable there instead
            PlayerPrefs.SetInt("exampleIntSave", exampleInt);
            PlayerPrefs.SetFloat("exampleFloatSave", exampleFloat);
            PlayerPrefs.Save();
        }

        void CollectSavedValues()
        {
            collectString = PlayerPrefs.GetString("exampleStringSave");
            collectInt = PlayerPrefs.GetInt("exampleIntSave");
            collectFloat = PlayerPrefs.GetFloat("exampleFloatSave");
        }

        void Awake()
        {
            Save();
            CollectSavedValues();
        }


        void Update()
        {
            Debug.Log(collectString);
            Debug.Log(collectInt);
            Debug.Log(collectFloat);
        }
    }


}





