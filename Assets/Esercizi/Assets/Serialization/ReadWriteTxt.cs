﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Lezione10
{

public class ReadWriteTxt : MonoBehaviour
{
    string path;
        Vector3 posizione;
    void Start()
    {
        path= Application.persistentDataPath + "/test.txt"; 
        WriteString(path, posizione.ToString());
        Debug.Log(ReadString(path));
    }

    static void WriteString(string path, string text)
    {
        StreamWriter writer = new StreamWriter(path, true);
        writer.WriteLine(text);
        writer.Close();
    }

    static string ReadString(string path)
    {
        //Read the text from directly from the test.txt file
        StreamReader reader = new StreamReader(path);
        string text = reader.ReadToEnd();
        
        reader.Close();
        return text;
    }
}
}

