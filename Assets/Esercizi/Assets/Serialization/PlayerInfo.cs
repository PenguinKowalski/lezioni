﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Lezione10
{
    [System.Serializable]
    public class PlayerInfo:MonoBehaviour
    {
        string path;

        public string[] names;
        public int[] scores;

        public static PlayerInfo CreateFromJSON(string jsonString)
        {
            return JsonUtility.FromJson<PlayerInfo>(jsonString);
        }

        public bool FromJson(string jsonString)
        {
            JsonUtility.FromJsonOverwrite(jsonString,this);
  

            return true;

            
        }

        public string ToJson()
        {
            return JsonUtility.ToJson(this);
        }

        void Start()
        {
            path = Application.persistentDataPath + "/test.txt";         
        }

        public void Save()
        {
            WriteString(path, ToJson());
            Debug.Log("saved!");
        }
        public void Load()
        {
            FromJson(ReadString(path));
            Debug.Log("loaded!");
        }


        static void WriteString(string path, string text)
        {
            StreamWriter writer = new StreamWriter(path, false);
            writer.WriteLine(text);
            writer.Close();
        }

        static string ReadString(string path)
        {
            StreamReader reader = new StreamReader(path);
            string text = reader.ReadToEnd();

            reader.Close();
            return text;
        }
    }
}
