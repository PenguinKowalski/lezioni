﻿using UnityEngine;

namespace Lezione10
{
    [CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/SpawnManagerSO", order = 1)]
    public class SpawnManagerSO : ScriptableObject
    {
        public string prefabName;

        public int numberOfPrefabsToCreate;
        public Vector3[] spawnPoints;
    }
}