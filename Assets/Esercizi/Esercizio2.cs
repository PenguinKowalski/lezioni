using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Esercizio2 : MonoBehaviour
{

    [SerializeField] List<Persona> listaPersone = new List<Persona>();

    void Start()
    {
       // Persona personadaaggiungere = new Persona("Mario", "Rossi", "Via Milano 1", 10);

       // listaPersone.Add(personadaaggiungere);

        int etaMin = int.MaxValue;
        Persona personagiovane=null;

        for (int i=0; i< listaPersone.Count; i++)
        {
            if (listaPersone[i].eta < etaMin)
            {
                etaMin = listaPersone[i].eta;
                personagiovane = listaPersone[i];
            }
        }


        // Debug.Log("la persona piu' giovane �" + personagiovane.nome + " " + personagiovane.cognome);
        if (personagiovane != null)
        {
            Debug.Log("la persona piu' giovane � " + personagiovane);
        }
        else
        {
            Debug.Log("devi mettere almeno una persona per funziona, scemo");
        }



    }

}

[System.Serializable]
public class Persona
{
    public string nome, cognome, indirizzo;
    public int eta;

    public Persona(string nome, string cognome, string indirizzo, int eta)
    {
        this.nome = nome;
        this.cognome = cognome;
        this.indirizzo = indirizzo;
        this.eta = eta;
    }

    public override string ToString()
    {
        return nome + " " + cognome + " : " + eta;
    }
}
