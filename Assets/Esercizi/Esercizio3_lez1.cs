using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Esercizio3_lez1 : MonoBehaviour
{
    public int grandezzaPiramide = 10;
    private string carattere = "*";

    string piramide = "";
    void Start()
    {
        /* ALTERNATIVA ALTEZZA
        for (int i=0;  i< grandezzaPiramide; i++)
        {
            piramide = piramide + carattere;
            Debug.Log(piramide);
        }

        for (int i = 0; i < grandezzaPiramide-1; i++)
        {
          
            Debug.Log(piramide);
        }
        */
        bool primavolta = false;
        //ALTERNATIVA LARGHEZZA
        for (int i = 0; i < grandezzaPiramide; i++)
        {
            if (i< Mathf.CeilToInt((float)grandezzaPiramide / 2f))
            {
                piramide = piramide + carattere;
            }
            else
            {
                if (grandezzaPiramide%2 ==0 && !primavolta) //caso pari
                {
                    primavolta = true;
                }
                else
                {
                    piramide = piramide.Remove(piramide.Length - carattere.Length, carattere.Length);
                }           
            }

            Debug.Log(piramide);
        }
    }

}
