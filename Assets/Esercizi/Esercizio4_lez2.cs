using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Esercizio4_lez2 : MonoBehaviour
{
    char a = 'X', b = 'O';
    [SerializeField] int larg=5, lung=5;

    string stringatotale;
    // Start is called before the first frame update
    void Start()
    {      
        for (int i=0; i< larg; i++)
        {
            bool riga_dispari = false;
            if (i % 2 != 0) //dispari
            {
                riga_dispari = true;
            }

            string riga = "";
            for (int j = 0; j < lung; j++)
            {
                bool j_dispari = false;
                if (j % 2 != 0) //dispari
                {
                    j_dispari = true;
                }

                if (riga_dispari)
                {
                    if (j_dispari)
                    {
                        riga += a;
                    }
                    else
                    {
                        riga += b;
                    }                 
                }
                else
                {
                    if (j_dispari)
                    {
                        riga += b;
                    }
                    else
                    {
                        riga += a;
                    }
                }
                 

            }
            //Debug.Log(riga);
            stringatotale = stringatotale + riga + "\n";
        }
        Debug.Log(stringatotale);
    }


}
