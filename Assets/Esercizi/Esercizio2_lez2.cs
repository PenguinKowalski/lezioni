using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Esercizio2_lez2 : MonoBehaviour
{
    /* ALTERNATIVA 1
    public List<Animale> listaAnimali;
    // Start is called before the first frame update
    void Start()
    {
        int somma_zampe = 0;
        for (int i=0; i < listaAnimali.Count; i++){

            if (listaAnimali[i].flag_scelto)
            {
                somma_zampe = somma_zampe + listaAnimali[i].numero_zampe;
            }
        }

        Debug.Log("la somma delle zampe degli animali scelti � " + somma_zampe);
    }
    */


    //ALTERNATIVA 2

    public List<Animale> listaAnimali = new List<Animale>();
    public List<AnimaleScelto> animaliScelti = new List<AnimaleScelto>();

    public Dictionary<string, Animale> TabellaAnimali = new Dictionary<string, Animale>();

    void Start()
    {
        for (int i = 0; i < listaAnimali.Count; i++)
        {
            TabellaAnimali.Add(listaAnimali[i].nome, listaAnimali[i]);
        }

       
        int somma_zampe = 0;
        bool trovaterrori = false;
        for (int i = 0; i < animaliScelti.Count; i++)
        {
            if (TabellaAnimali.ContainsKey(animaliScelti[i].nomeAnimale))
            {
                somma_zampe += TabellaAnimali[animaliScelti[i].nomeAnimale].numero_zampe;
            }
            else
            {
                Debug.Log("trovato animale ineistenza");
                return;
                //trovaterrori = true;
            }
        }

       // if (!trovaterrori)
        {
            Debug.Log("la somma delle zampe degli animali scelti � " + somma_zampe);
        }
        
    }
}

[System.Serializable]
public class Animale
{
    public string nome;
    public int numero_zampe;
   /*ALTERNATIVA 1 public bool flag_scelto;*/
}

[System.Serializable]
public class AnimaleScelto
{
    public string nomeAnimale;
}
