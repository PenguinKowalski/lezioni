using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LezioneUI : MonoBehaviour
{
    public GameObject prefabBottone, parente;
    Button button;
    // Start is called before the first frame update
    void Start()
    {
       GameObject go=  GameObject.Instantiate(prefabBottone, parente.transform);
        button = go.GetComponent<Button>();

        button.onClick.AddListener(()=> 
        {
            ChiamataFunzione_OnClick(0);
        }
        );
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChiamataFunzione_OnClick(int valore)
    {
        Debug.Log("ho cliccato il bototne " + valore);
    }
}
