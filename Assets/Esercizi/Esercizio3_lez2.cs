using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Esercizio3_lez2 : MonoBehaviour
{
    [SerializeField]
    int numeroMassimo = 20;
    void Start()
    {
        for (int i = 0; i < numeroMassimo; i++)
        {
            if (i % 2 != 0)
            {
                Debug.Log(i);
            }
        }

    }


}
