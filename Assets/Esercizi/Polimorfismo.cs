using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace esempioPoly
{


public class Polimorfismo : MonoBehaviour
{
    public List<Persona> listaPersone;
    // Start is called before the first frame update
    void Start()
    {
         listaPersone.Add(new Persona("Mario", "Rossi", 12));
         listaPersone.Add(new Studente("Luigi", "Verdi", 6, "galileo ferraris"));
         listaPersone.Add(new Persona("Gabriele", "Arancione", 45));

            for (int i=0; i< listaPersone.Count; i++)
        {
                Debug.Log(listaPersone[i].GetPersona());
        }
    }


    }
}
